var group__bitoperations =
[
    [ "concatenate_bytes", "group__bitoperations.html#gabd29908e4c520131787129fc0879329f", null ],
    [ "concatenate_nibbles", "group__bitoperations.html#ga23fcf15b4138e6a87af8d8594bdf6d8a", null ],
    [ "extract_bit", "group__bitoperations.html#ga40ea44480a08fe2dc6b0c193b6b8555f", null ],
    [ "extract_digit", "group__bitoperations.html#ga761e851a4f24fdc833e71585956ce758", null ],
    [ "extract_nibble", "group__bitoperations.html#ga8dcf85e0166dc9a49ee3800fd9f205f0", null ]
];