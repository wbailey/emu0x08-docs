var structchip__8 =
[
    [ "cpu_clock", "structchip__8.html#afabaec92729ffbfab5113feecc8fb24e", null ],
    [ "delay_clock", "structchip__8.html#a27040a538dd0d0a7406d43d66daff8f1", null ],
    [ "delay_timer", "structchip__8.html#a8058584b3bb79969f224d21379562ae6", null ],
    [ "index", "structchip__8.html#a0d6aa62fc9b2f70dcfb61f2286a52f1f", null ],
    [ "keys", "structchip__8.html#a2200a0f572dbf676a64e42d70d41385b", null ],
    [ "memory", "structchip__8.html#aedfbb7285e8be62152b31247c6e26f8b", null ],
    [ "program_counter", "structchip__8.html#a62485be0fe51b6544f550fb106e9755c", null ],
    [ "registers", "structchip__8.html#abc9922a880ef9b8c35375ab179867582", null ],
    [ "sound_clock", "structchip__8.html#ae9830423cef5d42f7b266ed18685d238", null ],
    [ "sound_timer", "structchip__8.html#a61e8b2293e3c1c2773e488977ead6c00", null ],
    [ "stack", "structchip__8.html#a6a1a4912c0634a9110e577ece47c8221", null ],
    [ "stack_pointer", "structchip__8.html#a72b9df7ca2abf9eff9f894ef72145e0d", null ],
    [ "video_memory", "structchip__8.html#af1921299457fb5be090e21bdd02d11f8", null ]
];