var modules =
[
    [ "Bit Operations", "group__bitoperations.html", "group__bitoperations" ],
    [ "Chip 8", "group__chip__8.html", "group__chip__8" ],
    [ "Clock and Timer", "group__clock__timer.html", "group__clock__timer" ],
    [ "Globals", "group__globals.html", "group__globals" ],
    [ "Input Output", "group__Input__Output.html", "group__Input__Output" ],
    [ "Memory Array", "group__memory__array.html", "group__memory__array" ],
    [ "Pixel Matrix", "group__pixel__matrix.html", "group__pixel__matrix" ]
];