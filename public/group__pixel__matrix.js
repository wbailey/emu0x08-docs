var group__pixel__matrix =
[
    [ "pixel_matrix", "structpixel__matrix.html", [
      [ "data", "structpixel__matrix.html#a2d945149fb67f102649363a38e108419", null ],
      [ "m", "structpixel__matrix.html#a4782d86e4aa26efcbf7d69e38e6aa309", null ],
      [ "n", "structpixel__matrix.html#a377fbee353ae5135f227f21f5445c06b", null ]
    ] ],
    [ "create_pixel_matrix", "group__pixel__matrix.html#ga1dd58ea9ca618fe142d93761f85a5bd5", null ],
    [ "destroy_pixel_matrix", "group__pixel__matrix.html#ga90fccd5a6c6ff1acd561df90079c532f", null ],
    [ "pixel_matrix_clear", "group__pixel__matrix.html#ga3fa8a30d17ad154bd5a323c20a08a22d", null ],
    [ "pixel_matrix_get_pixel", "group__pixel__matrix.html#gac97b3796ce1a24306be1f013eb77857e", null ],
    [ "pixel_matrix_print", "group__pixel__matrix.html#gac95aa4d96b08ad2803d0b2816c44c184", null ],
    [ "pixel_matrix_set_all", "group__pixel__matrix.html#ga98db668559c60d97d01aae6bc986d278", null ],
    [ "pixel_matrix_set_pixel", "group__pixel__matrix.html#ga522aca3f1b7826a635c8b6ffd8947e02", null ]
];