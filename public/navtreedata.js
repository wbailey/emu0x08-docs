/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "emu0x08", "index.html", [
    [ "Project Goals", "index.html#autotoc_md1", [
      [ "MVP", "index.html#autotoc_md2", null ],
      [ "Stretch goals", "index.html#autotoc_md3", null ]
    ] ],
    [ "Dependancies", "index.html#autotoc_md4", null ],
    [ "Geting started", "index.html#autotoc_md5", [
      [ "Build commands", "index.html#autotoc_md6", null ],
      [ "Test Commnads", "index.html#autotoc_md7", null ],
      [ "emu0x08 Usage", "index.html#autotoc_md8", null ]
    ] ],
    [ "Chip-8 Specs", "index.html#autotoc_md9", [
      [ "Architechure", "index.html#autotoc_md10", null ],
      [ "opcodes", "index.html#autotoc_md11", null ]
    ] ],
    [ "Scratchpad", "index.html#autotoc_md12", [
      [ "Todos", "index.html#autotoc_md13", [
        [ "Roms to test with", "index.html#autotoc_md14", null ]
      ] ]
    ] ],
    [ "Sources", "index.html#autotoc_md15", [
      [ "General/Tech Ref", "index.html#autotoc_md16", null ],
      [ "Roms", "index.html#autotoc_md17", null ]
    ] ],
    [ "Todo List", "todo.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';