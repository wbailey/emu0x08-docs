var group__clock__timer =
[
    [ "clock", "structclock.html", [
      [ "elapsed", "structclock.html#a94588ed644556c1f24accc6832b21756", null ],
      [ "hz", "structclock.html#a363c2a24407a97ef27aa850fa40d9b94", null ]
    ] ],
    [ "timer", "structtimer.html", [
      [ "current_time", "structtimer.html#a51a5cfd784c599a60d51afd263be7861", null ],
      [ "delta", "structtimer.html#a6b5112b132a2e49ee8077ab601291e36", null ],
      [ "previous_time", "structtimer.html#a75dc745ea8f89b2e8d667eaa650210ae", null ]
    ] ],
    [ "clock_update", "group__clock__timer.html#gac2c24f4e53ba5de1c4d068aca4c95291", null ],
    [ "create_clock", "group__clock__timer.html#gaa471be45118446753eb51696b1f9f499", null ],
    [ "create_timer", "group__clock__timer.html#gac99a2798e3050d9c3b0811d230475f8d", null ],
    [ "destroy_clock", "group__clock__timer.html#ga6bccfe65af6e1761a83a9e3b2fc8e706", null ],
    [ "destroy_timer", "group__clock__timer.html#gaf1ed7ac353b95f342494b4cb1b45fee0", null ],
    [ "timer_calc_delta", "group__clock__timer.html#gadc97679b0883cefd8983ac194841e3c1", null ]
];