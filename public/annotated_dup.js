var annotated_dup =
[
    [ "chip_8", "structchip__8.html", "structchip__8" ],
    [ "clock", "structclock.html", "structclock" ],
    [ "globals", "structglobals.html", "structglobals" ],
    [ "memory_array", "structmemory__array.html", "structmemory__array" ],
    [ "pixel_matrix", "structpixel__matrix.html", "structpixel__matrix" ],
    [ "timer", "structtimer.html", "structtimer" ]
];