var indexSectionsWithContent =
{
  0: "bcdeghilmnprstvw",
  1: "cgmpt",
  2: "cdehmprt",
  3: "cdehlmnpsv",
  4: "bcgimp",
  5: "et"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "functions",
  3: "variables",
  4: "groups",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Functions",
  3: "Variables",
  4: "Modules",
  5: "Pages"
};

