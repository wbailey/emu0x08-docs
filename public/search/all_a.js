var searchData=
[
  ['pixel_20matrix_54',['Pixel Matrix',['../group__pixel__matrix.html',1,'']]],
  ['pixel_5fmatrix_55',['pixel_matrix',['../structpixel__matrix.html',1,'']]],
  ['pixel_5fmatrix_5fclear_56',['pixel_matrix_clear',['../group__pixel__matrix.html#ga3fa8a30d17ad154bd5a323c20a08a22d',1,'pixel_matrix.c']]],
  ['pixel_5fmatrix_5fget_5fpixel_57',['pixel_matrix_get_pixel',['../group__pixel__matrix.html#gac97b3796ce1a24306be1f013eb77857e',1,'pixel_matrix.c']]],
  ['pixel_5fmatrix_5fprint_58',['pixel_matrix_print',['../group__pixel__matrix.html#gac95aa4d96b08ad2803d0b2816c44c184',1,'pixel_matrix.c']]],
  ['pixel_5fmatrix_5fset_5fall_59',['pixel_matrix_set_all',['../group__pixel__matrix.html#ga98db668559c60d97d01aae6bc986d278',1,'pixel_matrix.c']]],
  ['pixel_5fmatrix_5fset_5fpixel_60',['pixel_matrix_set_pixel',['../group__pixel__matrix.html#ga522aca3f1b7826a635c8b6ffd8947e02',1,'pixel_matrix.c']]],
  ['previous_5ftime_61',['previous_time',['../structtimer.html#a75dc745ea8f89b2e8d667eaa650210ae',1,'timer']]],
  ['print_5fbuffer_62',['print_buffer',['../group__Input__Output.html#ga183e204a44b1cdfd10294c824a6f048f',1,'input_output.c']]]
];
