var searchData=
[
  ['chip_208_1',['Chip 8',['../group__chip__8.html',1,'']]],
  ['chip_5f8_2',['chip_8',['../structchip__8.html',1,'']]],
  ['chip_5f8_5fcpu_5fcycle_3',['chip_8_cpu_cycle',['../group__chip__8.html#ga1c3b5f482ef57028fc3349db7235a7c1',1,'chip_8.c']]],
  ['chip_5f8_5fdecrement_5fprogram_5fcounter_4',['chip_8_decrement_program_counter',['../group__chip__8.html#ga5154882013ca8f29c69cf069bff135ca',1,'chip_8.c']]],
  ['chip_5f8_5fexecute_5fnext_5finstruction_5',['chip_8_execute_next_instruction',['../group__chip__8.html#ga59f60bb62166771fccac0ba12e6e3d51',1,'chip_8.c']]],
  ['chip_5f8_5fincrement_5fprogram_5fcounter_6',['chip_8_increment_program_counter',['../group__chip__8.html#ga20ae99a3c9fe7742f47cc8201a8daff8',1,'chip_8.c']]],
  ['chip_5f8_5fload_5from_7',['chip_8_load_rom',['../group__chip__8.html#gaa877f9f6bef11cbac0cefcb5612ff6f4',1,'chip_8_load_rom(struct chip_8 *self, uint8_t *buffer, size_t size, int address):&#160;chip_8.c'],['../group__chip__8.html#gaa877f9f6bef11cbac0cefcb5612ff6f4',1,'chip_8_load_rom(struct chip_8 *self, uint8_t *buffer, size_t size, int address):&#160;chip_8.c']]],
  ['clock_8',['clock',['../structclock.html',1,'']]],
  ['clock_20and_20timer_9',['Clock and Timer',['../group__clock__timer.html',1,'']]],
  ['clock_5fspeed_10',['clock_speed',['../structglobals.html#a8a8eabaf57d715ccdef9080e91875dd8',1,'globals']]],
  ['clock_5fupdate_11',['clock_update',['../group__clock__timer.html#gac2c24f4e53ba5de1c4d068aca4c95291',1,'clock_timer.c']]],
  ['concatenate_5fbytes_12',['concatenate_bytes',['../group__bitoperations.html#gabd29908e4c520131787129fc0879329f',1,'bitoperations.c']]],
  ['concatenate_5fnibbles_13',['concatenate_nibbles',['../group__bitoperations.html#ga23fcf15b4138e6a87af8d8594bdf6d8a',1,'bitoperations.c']]],
  ['create_5fchip_5f8_14',['create_chip_8',['../group__chip__8.html#gaa73c6d2f9d32d3878a125677269b7952',1,'create_chip_8(float clock_speed):&#160;chip_8.c'],['../group__chip__8.html#gaa73c6d2f9d32d3878a125677269b7952',1,'create_chip_8(float clock_speed):&#160;chip_8.c']]],
  ['create_5fclock_15',['create_clock',['../group__clock__timer.html#gaa471be45118446753eb51696b1f9f499',1,'clock_timer.c']]],
  ['create_5fmemory_5farray_16',['create_memory_array',['../group__memory__array.html#ga93cbf33c59a4b29df0d2826a0beb9374',1,'memory_array.c']]],
  ['create_5fpixel_5fmatrix_17',['create_pixel_matrix',['../group__pixel__matrix.html#ga1dd58ea9ca618fe142d93761f85a5bd5',1,'pixel_matrix.c']]],
  ['create_5ftimer_18',['create_timer',['../group__clock__timer.html#gac99a2798e3050d9c3b0811d230475f8d',1,'clock_timer.c']]],
  ['current_5ftime_19',['current_time',['../structtimer.html#a51a5cfd784c599a60d51afd263be7861',1,'timer']]]
];
