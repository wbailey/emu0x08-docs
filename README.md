# Emu0x08 Documentation

This is the documentation for [emu0x08](https://gitlab.com/wbailey/emu0x08)'s API. 

[Here](https://wbailey.gitlab.io/emu0x08-docs/) is link to the live documentation site.


```python
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@B##B#&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@#55G&&B#@@@@@@&#&@@@@@@&@@&@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@B55B#BPPG#@@&##BG&&@@&@@@#P&@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@#P555YYYJYY5YY5PY5B#GGBB###@@@@@@@@@@@@@@@
@@@@@@@@@@@&@&@@&&#55PPYJ55Y?YPPP5Y?5Y55YYPGB&&&&@&@@@@@@@@@
@@@@@@@@@@@#&&##BGPGPP5JJJY5555PB5YJJY5YJYYY5555GBG&@@@@@@@@
@@@@@@@@@@#GPB55YYYYYYYY????J555PJJYYY55PPGPPGPGGB&@@@@@@@@@
@@@@@@@@@@#J7?JJYYYJYYJJJ??JJJYYYYJYYJYYPPP5PGBBG#&@@@@@@@@@
@@@@@@@@@@&PJ?JYJ?77??YJ??Y??JJ?JJYJJ??JYYP55G##GBB&@@@@@@@@
@@@@@@@@@@@BPYYJ7~~~77?JJ5YY55YY5GPGP5YJY5PPGPB##BB#@@@@@@@@
@@@@@@@@@@&#GY7~:.^^~!?JYPGGGBGPB5##GPBGPPP5PGB###@@@@@@@@@@
@@@@@@@@@@&5!~:.:^~!7?J5PGP5GGBGBPBBY5&#GP55JY5PG#&@@@@@@@@@
@@@@@@@@@P!^!?7?5PP555GBGB5YY5YGBP555PP55YY5Y??Y5G&@@@@@@@@@
@@@@@@@P~^~!YGBBYJJ!7Y5555PPPP5PPP5YJJJY5YYJJJY55G&@@@@@@@@@
@@@@@B?^~~~~~7??JJY5PGBBB#BBBGBGGBBG5P5PPGP555555B&@@@@@@@@@
@@@@@?:!Y55PPGBB###&&&&#BBGPPPGPPGPPPP5PPGPGG5PPG#@@@@@@@@@@
@@@@@GJ5B##&####BGPPPY5PGBGPPGBBGGGGPGGGGPPG5JGB###@@@@@@@@@
@@@@@@@@@@@@@@@&&#BBBP?!J5P5PGPPPJYY5PGP55Y?77G##&&@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@&#Y!~7JYYPP55?77?JYYJ?!!!7G#&@&@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@&57!!??JY5Y?!^77JJJ7!!!!P###@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@&57!~!!7JY??7^JJY?J?7~!7P#&&@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@P7~^~!!7?7J?7?JY??7~!7?G##&&&&@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@B5~^^~!!!7!~7!?5J~!~!7JBB##&@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@&#BY~:^7J?!!!!~?PP7~!^7JB###@@@@@@@@@@
```
